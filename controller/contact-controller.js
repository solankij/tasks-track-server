const Contact = require('../models/contact');


// all contacts
exports.contact_all = (req, res) => {
    Contact.find({}).exec((err, contacts) => {
        if (err) {
            res.json({
                success: false,
                error: err
            })
        } else {
            res.json({
                success: true,
                contacts: contacts
            });
        }
    });
}

exports.contact_list = (req, res) => {
    let search = {
        $match: {
            $or: [{
                    firstName: {
                        "$regex": req.query.search
                    }
                },
                {
                    lastName: {
                        "$regex": req.query.search
                    }
                }
            ]
        }
    }

    let pageLimit = {
        $limit: Number(req.query.pageSize)
    }
    let pageToSend = {
        $skip: (Number(req.query.pageIndex) * Number(req.query.pageSize))
    }
    Contact.aggregate([{
        $facet: {
            values: [search, pageToSend, pageLimit],
            total: [search, {
                $count: "total"
            }]
        }
    }]).exec((err, contacts) => {
        if (err) {
            res.json({
                success: false,
                error: err
            })
        } else {
            if (contacts[0].values.length > 0) {
                res.json({
                    total: contacts[0].total[0].total,
                    values: contacts[0].values
                });
            } else {
                res.json({
                    total: 0,
                    values: []
                });
            }
        }
    });
}

exports.contact_create = (req, res) => {
    const contact = new Contact({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        phone: req.body.phone
    })
    contact.save((err, contact) => {
        if (err) {
            res.json({
                sucess: false,
                message: err
            })
        } else {
            res.json({
                success: true,
                message: "contact saved successfully"
            })
        }
    })

}

exports.contact_edit = (req, res) => {
    let contact = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        phone: req.body.phone
    }

    Contact.findOneAndUpdate({
        _id: req.params.id
    }, contact, (err, contactObj) => {
        if (err) {
            res.json({
                success: false,
                error: err
            })
        } else {
            res.json({
                success: true,
                contact: contact
            })
        }
    })
}

exports.contact_remove = (req, res) => {
    Contact.findOneAndRemove({
        _id: req.params.id
    }, (err, contactObj) => {
        if (err) {
            res.json({
                success: false,
                error: err
            })
        } else {
            res.json({
                success: true,
                task: contactObj
            })
        }
    })

    //  res.send("contact removed")
}