const jwt  = require('jsonwebtoken');
const User = require('../models/user');
const config = require('../config/database');

let userCheck = (req, res) => {
    User.findOne({
        username: req.body.username.toLowerCase()
    }, (err, user) => {
        if (err) {
            res.json({
                success: false,
                message: err
            })
        } else {
            if (!user) {
                res.json({
                    success: false,
                    message: "No user found with this username"
                });
            } else {
                passwordCheck(req, res, user);
            }
        }
    });
}

let passwordCheck = (req, res, user) => {
    const validPassword = user.comparePassword(req.body.password);
    if (!validPassword) {
        res.json({
            success: false,
            message: "password is invalid"
        });
    } else {
        const token = jwt.sign({
            userId: user._id
        }, config.secret, {
            expiresIn: '24h'
        });
        res.json({
            success: true,
            message: "login successfull!",
            token: token,
            user: {
                username: user.username
            }
        });
    }
}

exports.login = (req, res) => {
    if (!req.body.username) {
        res.json({
            success: false,
            message: "No username provided"
        });
    } else if (!req.body.password) {
        res.json({
            success: false,
            message: "No password provided!"
        });
    } else {
        userCheck(req, res);
    }
}

exports.authCheck = (req, res, next) => {
    const token = req.headers['authorization'];
    if (!token) {
        res.json({
            success: false,
            message: "No token provided"
        });
    } else {
        jwt.verify(token, config.secret, (err, decoded) => {
            if (err) {
                res.json({
                    success: false,
                    message: "Invalid token provided " + err
                })
            } else {
                req.decoded = decoded;
                next();
            }
        });
    }
}