const Task = require('../models/task');
const moment = require('moment');

/** Todo - improve aggragation with readable formate */
exports.task_all = (req,res) => {
    res.json({success:true});
}
//get the task list based on query
exports.task_list = (req, res) => {
    let findDateFrom = moment(new Date(moment.unix(req.query.date).format("YYYY-MM-DD HH:mm"))).add(1, "day").format("YYYY-MM-DD HH:mm")
    let search = {
        $match: {
            note: {
                "$regex": req.query.search
            }
        }
    }
    let dateSearch = {
        $match: {
            date: {
                "$gte": new Date(moment.unix(req.query.date).format("YYYY-MM-DD HH:mm")),
                "$lt": new Date(findDateFrom)
            }
        }
    }

    let pageLimit = {
        $limit: Number(req.query.pageSize)
    }
    let pageToSend = {
        $skip: (Number(req.query.pageIndex) * Number(req.query.pageSize))
    }
    Task.aggregate([{
        $facet: {
            values: [dateSearch, search, pageToSend, pageLimit],
            total: [dateSearch, search, {
                $count: "total"
            }]
        }
    }]).exec((err, tasks) => {
        if (err) {
            res.json({
                success: false,
                error: err
            })
        } else {
            if (tasks[0].values.length > 0) {
                res.json({
                    total: tasks[0].total[0].total,
                    values: tasks[0].values
                })
            } else {
                res.json({
                    total: 0,
                    values: []
                })
            }
        }
    })
}


/* exports.task_by_date = (req, res) => {
    let findDateFrom = moment(new Date(req.params.date)).add(1, "day").format("YYYY-MM-DD HH:mm")
    Task.find({
        date: {
            "$gte": new Date(req.params.date).toISOString(),
            "$lt": new Date(findDateFrom).toISOString()
        }
    }).exec((err, tasks) => {
        if (err) {
            res.json({
                success: false,
                error: "tasks not available for this record"
            })
        } else {
            res.json({
                success: true,
                taskList: tasks
            })
        }
    })
} */


exports.task_by_date = (req, res) => {
    let searchT = req.query.search
    let pageIndex = req.query.pageIndex || 1
    let pageSize = req.query.pageSize || 10

    let findDateFrom = moment(new Date(req.query.date)).add(1, "day").format("YYYY-MM-DD HH:mm")
    Task.aggregate([{
        $match: {
            date: {
                "$gte": moment(new Date(req.query.date)).utc(true).format(),
                "$lt": moment(new Date(findDateFrom)).utc(true).format()
            },
            note: {
                "$regex": searchT
            }
        }
    }]).exec((err, tasks) => {
        if (err) {
            res.json({
                success: false,
                error: "tasks not available for this record"
            })
        } else {
            res.json({
                success: true,
                taskList: tasks
            })
        }
    })
}

// create task by date
exports.task_create = (req, res) => {
    const task = new Task({
        date: moment(new Date(req.body.date)).utc().format(),
        time: req.body.time,
        note: req.body.note,
        status: req.body.status,
        contactId: req.body.contactId
    })
    task.save((err, task) => {
        if (err) {
            res.json({
                success: false,
                message: err
            })
        } else {
            res.json({
                success: true,
                message: "task saved successfully"
            })
        }
    })
}

//edit task change date field as well for time
exports.task_edit = (req, res) => {
    let task = {
        date: new Date(req.body.date),
        note: req.body.note,
        time: req.body.time,
        contactId: req.body.contactId,
        status: req.body.status
    }
    Task.findOneAndUpdate({
        _id: req.params.id
    }, task, (err, taskObj) => {
        if (err) {
            res.json({
                success: false,
                error: err
            })
        } else {
            res.json({
                success: true,
                task: taskObj
            })
        }
    })
}

//task delete
exports.task_delete = (req, res) => {
    Task.findOneAndRemove({
        _id: req.params.id
    }, (err, taskObj) => {
        if (err) {
            res.json({
                success: false,
                error: err
            })
        } else {
            res.json({
                success: true,
                task: taskObj
            })
        }
    })
}