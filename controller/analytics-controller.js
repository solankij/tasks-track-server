const Task = require('../models/task');
const moment = require('moment');

exports.shift_by_date = (req, res) => {
    const shift1 = moment(new Date(req.params.date)).hour(0);
    const shift2 = moment(new Date(req.params.date)).hour(6);
    const shift3 = moment(new Date(req.params.date)).hour(12);
    const shift4 = moment(new Date(req.params.date)).hour(18);
    let findDateFrom = moment(new Date(req.params.date)).add(1, "day").format("YYYY-MM-DD HH:mm")
    Task.find({
        date: {
            "$gte": new Date(req.params.date).toISOString(),
            "$lt": new Date(findDateFrom).toISOString()
        }
    }).exec((err, tasks) => {
        if (err) {
            res.json({
                success: false,
                error: "tasks not available for this record"
            });
        } else {

            const firstShift = tasks.filter(sf1 => moment(sf1.date).isSameOrBefore(shift2));
            const secondShift = tasks.filter(sf2 => moment(sf2.date).isSameOrBefore(shift3) && moment(sf2.date).isSameOrAfter(shift2));
            const thirdShift = tasks.filter(sf3 => moment(sf3.date).isSameOrBefore(shift4) && moment(sf3.date).isSameOrAfter(shift3));

            const fourthShift = tasks.filter(sf4 => moment(sf4.date).isSameOrAfter(shift4));

            res.json({
                success: true,
                taskList: {
                    s1: firstShift.length,
                    s2: secondShift.length,
                    s3: thirdShift.length,
                    s4: fourthShift.length
                }
            });
        }
    })
}

//task shift by date range

exports.shift_by_range = (req, res) => {
    let dateTo = moment(new Date(req.body.to)).format("YYYY-MM-DD HH:mm");

    Task.find({
        date: {
            "$gte": new Date(req.body.from).toISOString(),
            "$lt": new Date(dateTo).toISOString()
        }
    }).exec((err, tasks) => {
        if (err) {
            res.json({
                success: false,
                error: "tasks not available for this record"
            });
        } else {
            const firstShift = tasks.filter(sf1 => {
                let shift2 = moment(new Date(sf1.date)).hour(6);
                return moment(sf1.date).isSameOrBefore(shift2);
            });
            const secondShift = tasks.filter(sf2 => {
                let shift2 = moment(new Date(sf2.date)).hour(6);
                let shift3 = moment(new Date(sf2.date)).hour(12);
                return (moment(sf2.date).isSameOrBefore(shift3) && moment(sf2.date).isSameOrAfter(shift2));
            });
            const thirdShift = tasks.filter(sf3 => {
                let shift3 = moment(new Date(sf3.date)).hour(12);
                let shift4 = moment(new Date(sf3.date)).hour(18);
                return moment(sf3.date).isSameOrBefore(shift4) && moment(sf3.date).isSameOrAfter(shift3)
            });
            const fourthShift = tasks.filter(sf4 => {
                let shift4 = moment(new Date(sf4.date)).hour(18);
                return moment(sf4.date).isSameOrAfter(shift4);
            });

            res.json({
                success: true,
                taskList: {
                    s1: firstShift.length,
                    s2: secondShift.length,
                    s3: thirdShift.length,
                    s4: fourthShift.length
                }
            });
        }
    });
}