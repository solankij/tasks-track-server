const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;

// check email lenght
let emailLengthChecker = (email) => {
    if (email.length == 0) {
        return false;
    } else {
        if (email.length < 5 || email.length > 40) {
            return false;
        } else {
            return true;
        }
    }
};

//check if email is valid or not
let validEmailChecker = (email) => {
    if (email.length == 0) {
        return false;
    } else {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
};

const emailValidators = [{
    validator: emailLengthChecker,
    message: 'Email must be at lease 5 characters but no more than 40 characters'
},
{
    validator: validEmailChecker,
    message: 'invalid email'
}
];

const ContactSchema = new Schema({
    firstName: {
        type: String,
        required: true,
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        validate: emailValidators
    },
    phone: {
        type: Number,
        required: true,
    }
});
module.exports = mongoose.model('Contact', ContactSchema)