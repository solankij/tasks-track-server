const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;


const taskSchema = new Schema({
    date: {
        type: Date,
        required: true
    },
    time: {
        type: String,
        required: true,
        lowercase: true
    },
    note: {
        type: String,
        required: true,
    },
    contactId: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true  
    }
});
module.exports = mongoose.model('Task', taskSchema);