const taskCtrl = require('../controller/task-controller');
const analyticsCtrl = require('../controller/analytics-controller');
const contactCtrl = require('../controller/contact-controller');
const authCtrl = require('../controller/auth-controller');
const registerCtrl = require('../controller/register-controller');


module.exports = (router) => {
    router.use('/tracker', authCtrl.authCheck);

    //authentication
    router.post('/login', authCtrl.login);

    //register user
    router.post('/register', registerCtrl.register);

    //task routes
    router.get('/tracker/task/all', taskCtrl.task_all);
    router.get('/tracker/task/list', taskCtrl.task_list);
    router.post('/tracker/task/create', taskCtrl.task_create);
    router.put('/tracker/task/edit/:id', taskCtrl.task_edit);
    router.delete('/tracker/task/delete/:id', taskCtrl.task_delete);

    //analytics routes
    router.get('/analytics/:date', analyticsCtrl.shift_by_date);
    router.post('/analytics', analyticsCtrl.shift_by_range);

    //contact routes
    router.get('/tracker/contact/all', contactCtrl.contact_all);
    router.get('/tracker/contact/list', contactCtrl.contact_list);
    router.post('/tracker/contact/create', contactCtrl.contact_create);
    router.put('/tracker/contact/edit/:id', contactCtrl.contact_edit);
    router.delete('/tracker/contact/delete/:id', contactCtrl.contact_remove);
    return router;
}