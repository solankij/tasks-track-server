const express = require('express');
const app = express();
const router = express.Router();
const mongoose = require('mongoose');
const config = require('./config/database');
const routes = require('./routes/routes')(router);
const bodyParser = require('body-parser');
const cors = require('cors');

mongoose.Promise = global.Promise;
mongoose.connect(config.url, {
    useNewUrlParser: true
}, (err) => {
    if (err) {
        console.log('could not connect to database:', err);
    } else {
        console.log('connected to database', config.db);
    }
});

app.use(cors());


app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
app.use('/', routes);
let port = process.env.PORT || 8000;
app.listen(port, () => {
    console.log("Listening on port 8000.");
});